@extends('layouts.private.master')
@section('page-title', 'Users')

@section('script-header')
    
@endsection

@section('content')
<div id="app-view"></div>
@endsection

@section('script-footer')
    <script type="text/javascript" src="{!! asset(mix('js/users.build.js', 'users')) !!}"></script>
@endsection
