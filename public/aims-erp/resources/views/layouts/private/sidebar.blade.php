<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li>
        <a href="./dashboard">
          <i class="fas fa-tachometer-alt"></i> 
          <span>Dashboard </span>
        </a>
      </li>
      <li class="header">Settings</li>
      <li class="treeview">
        <a href="#">
        <i class="fas fa-user"></i> <span>User</span>
          <span class="pull-right-container">
            <i class="fas fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="./users"><i class="fas fa-circle"></i> Users</a></li>
          <li><a href="#"><i class="fas fa-circle"></i> User Access Level</a></li>
          <li><a href="#"><i class="fas fa-circle"></i> User Role</a></li>
          <li><a href="#"><i class="fas fa-circle"></i> User Group</a></li>
        </ul>
      </li>




    </ul>
  </section>
</aside>