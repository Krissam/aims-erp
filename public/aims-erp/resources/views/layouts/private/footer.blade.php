

@yield('script-footer')
<footer class="main-footer ">
    <div class="text-right hidden-xs">
      <b>Version</b> <?php echo env('APP_VERSION'); ?>
    </div>
</footer>