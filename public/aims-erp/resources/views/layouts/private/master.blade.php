<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
    @yield('page-title', env('WL_NAME'))
  </title>
  <meta 
    content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" 
    name="viewport"
  >
  <link 
    rel="stylesheet" 
    type="text/css" 
    media="screen" 
    href="{!! asset(mix('css/core.build.css', 'core')) !!}"
  >
  <link 
    rel="stylesheet" 
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:
          300,400,600,700,300italic,400italic,600italic"
  >
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
    @include('layouts.private.header')
    @include('layouts.private.sidebar')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
            @yield('page-title')
            </h1>
        </section>
        <section class="content">
            @yield('content')
        </section>
    </div>
    @include('layouts.private.footer')
    @include('layouts.private.sidebar-control')
</div>

<script 
  type="text/javascript" 
  src="{!! asset(mix('js/core.build.js', 'core')) !!}">
</script>

</body>
</html>
