@extends('layouts.public.master')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <a href="#">{{ __('Login') }}</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <form method="POST" action="{{ route('login') }}">
        @csrf
            @error('email')
                <div class="form-group has-error">
                <label class="control-label" for="inputError">
                    <i class="fa fa-times-circle-o"></i> 
                    {{ $message }}
                </label>
            @else
                <div class="form-group has-feedback">
            @enderror
            
            <input name="email" type="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

        </div>
            @error('password')
                <div class="form-group has-error">
                <label class="control-label" for="inputError">
                    <i class="fa fa-times-circle-o"></i> 
                    {{ $message }}
                </label>
            @else
                <div class="form-group has-feedback">
            @enderror
            <input name="password" type="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox">
                    <label>
                    <input class="form-check-input" 
                           type="checkbox" 
                           name="remember" 
                           id="remember" 
                           {{ old('remember') ? 'checked' : '' }}
                    >
                    {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                    {{ __('Login') }}
                </button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <!-- /.social-auth-links -->


    @if (Route::has('password.request'))
    <a class="btn btn-link" href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
    </a>
    @endif

    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
@endsection
