const defaultState = {
    today: true,
};
export default (state = defaultState , action) => { 
    switch(action.type) {
        case 'SET_USER':
            return  { ...state, today: action.payload};
        default:
            return state;
    }

}