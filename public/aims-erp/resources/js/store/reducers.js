import { combineReducers } from 'redux';

import Users from './reducers/users';

const Reducers = combineReducers({
    Users
});

export default Reducers;
