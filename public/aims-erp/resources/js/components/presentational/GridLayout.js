
import React from 'react';

const Row = (props) => {
    return (
        <div className="row">
            {props.children}
        </div>
    )
}

export const Column = (props) => {

    let classes = 'col-sm-12';
    if (props.classes) {
        classes = props.classes;
    }

    return (
        <div className={classes}>
            {props.children}
        </div>
    )
}

export default Row;