
import React from 'react';

const PortletDefault = (props) => {
    return (
        <div className="box box-solid">
            {Header(props)}
            <div className="box-body">
                {props.children}
            </div>
        </div>
    )
}


const Header = (props) => {
    if (props.title) {
        return (
            <div className="box-header with-border">
                <h3 className="box-title">{props.title}</h3>
                {ButtonRight(props)}
            </div>
        )
    }
    return null;
}


const ButtonRight = (props) => {
    if (props.subHeader) {
        return (
            <div class="box-tools pull-right">
                {props.subHeader}
            </div>
        )
    }
    return null;
}




export default PortletDefault;