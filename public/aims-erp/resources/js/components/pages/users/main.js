import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Store from '@store';
import UserList from './UserList';

ReactDOM.render((
    <Provider store={Store}>
        <UserList />
    </Provider>
), document.getElementById('app-view'))
