import React, { Component, Fragment } from 'react';
import { 
    PortletDefault,
    Row,
    Column
} from '@presentational';

class UserList extends Component {
    constructor (props){
        super(props);
        this.state = {
        }
    }

    componentWillMount(){
    }

    buttonRight() {
        return(
            <button className="btn btn-block btn-primary btn-flat btn-sm">
                Add User
            </button>
        )
    }

   render() {
        return (
            <PortletDefault
                title="User List"
                subHeader={this.buttonRight()}
            >
                User
            </PortletDefault>
        );
   }
}

export default UserList;
