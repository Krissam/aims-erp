const mix = require('laravel-mix');
const webpack = require('webpack');
const dirName = 'users'; 


mix 
   .setPublicPath('./public/'+ dirName)
   .webpackConfig(webpack => {
      return {
         plugins: [

         ],
         module: {
          
         },
         resolve: {
           extensions: ['*', '.js', '.jsx'],
           alias: {
              '@presentational': path.resolve(__dirname, '../resources/js/components/presentational'),
              '@services': path.resolve(__dirname, '../resources/js/services'),
              '@action': path.resolve(__dirname, '../resources/js/store/actions'),
              '@store': path.resolve(__dirname, '../resources/js/store/Store'),
              'sass': path.resolve(__dirname, '../resources/sass')
           }
         } 
      };
   })
   .js([
    './resources/js/components/pages/'+ dirName +'/main.js'
   ], './public/'+ dirName +'/js/users.build.js').version()

   .styles([
   ],'./public/'+ dirName +'/css/users.build.css').version()

   .copyDirectory([
   ], './public/'+ dirName +'/fonts')
   
   .sourceMaps(true, 'source-map');