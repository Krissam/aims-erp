const mix = require('laravel-mix');
const webpack = require('webpack');
const dirName = 'core'; 


mix 
   .setPublicPath('./public/'+ dirName)
   .webpackConfig(webpack => {
      return {
         // devtool: 'inline-source-map',
         plugins: [
            new webpack.ProvidePlugin({
                  $: 'jquery',
                  jQuery: 'jquery',
                  'window.jQuery': 'jquery',
            }),
            new webpack.IgnorePlugin(/^\.\/locale$/)
         ],
         module: {
            rules : [{
               test: require.resolve('jquery'),
               use: [{
                  loader: 'expose-loader',
                  options: 'jquery'
               },{
                  loader: 'expose-loader',
                  options: '$'
               },{
                  loader: 'expose-loader',
                  options: 'jQuery'
               }]
            }]
         },   
      };
   })
   .js([
      './node_modules/admin-lte/bower_components/jquery/dist/jquery.min.js',
      './node_modules/admin-lte/bower_components/bootstrap/dist/js/bootstrap.min.js',
      './node_modules/admin-lte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
      './node_modules/admin-lte/bower_components/fastclick/lib/fastclick.js',
      './resources/js/admin-lte.js'
   ], './public/'+ dirName +'/js/core.build.js').version()

   .styles([
      './node_modules/@fortawesome/fontawesome-free/css/all.min.css' ,
      './node_modules/admin-lte/bower_components/bootstrap/dist/css/bootstrap.min.css',
      './node_modules/admin-lte/bower_components/font-awesome/css/font-awesome.min.css',
      './node_modules/admin-lte/bower_components/Ionicons/css/ionicons.min.css',
      './node_modules/admin-lte/dist/css/AdminLTE.min.css',
      './node_modules/admin-lte/dist/css/skins/_all-skins.css',
      './resources/style/app.css'
   ],'./public/'+ dirName +'/css/core.build.css').version()

   // .sass(
   //    './resources/sass/app.scss'
   // ,'./public/'+ dirName +'/css/core1.build.css')

   .copyDirectory([
      './node_modules/admin-lte/bower_components/font-awesome/fonts/*',
      './node_modules/admin-lte/bower_components/bootstrap/dist/fonts/*'
   ], './public/'+ dirName +'/fonts')
   
   .copyDirectory([
      './node_modules/@fortawesome/fontawesome-free/webfonts/*'
   ], './public/'+ dirName +'/webfonts')

   .copyDirectory([
      './node_modules/admin-lte/dist/img/*',
   ], './public/'+ dirName +'/media')
    
   .sourceMaps(true, 'source-map');